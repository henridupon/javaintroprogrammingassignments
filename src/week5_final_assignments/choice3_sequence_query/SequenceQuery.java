
package week5_final_assignments.choice3_sequence_query;
/*
 * Copyright (c) Henri du Pon
 * Email Henridupon@gmail.com
 * All rights reserved.
 *
 * Final assignment 3: Developing a Fasta sequence query app 
 * Special challenge of this assignment: pattern search and applying filters 
 *
 */

import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.lang.StringBuilder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Henri du Pon
 * @version 0.0.1
 */
public final class SequenceQuery {
    boolean legitinput = true;
    boolean regex = false;
    boolean prosites = false;
    boolean summary = false;
    boolean help = false;
    boolean description = false;
    boolean id = false;
    boolean organism = false;
    boolean pprosite = false;
    boolean tcsv = false;
    
    String input = null;
    String to_csv = ";";
    String file = null;    
    String find_prosite = null;
    String find_regex = null;
    String find_id = null;
    String find_description = null; 
    String find_organism = null;  
    String sequencetype = null;    
    Integer numberofsequences = 0;
    Integer averagelength = 0;
    Integer totallength = 0;   
    String prosite;
    
    List<FastaEntry> fastaEntries = new ArrayList<>();
    
    /**
     * 
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
      
        SequenceQuery inputfile = new SequenceQuery(args);
    }

    /**
     * 
     * private constructor.
     */
    private SequenceQuery(String [] args) {
        try{
        for (int i = 0; i < args.length; i++) {

            switch (args[i]) {
                case "--infile":
                    input = args[i + 1];
                    checkPairs(input);
                    i++;
                    break;
                case "--summary":
                    summary = true;
                    break;
                case "--find_prosite":
                    find_prosite = args[i + 1];
                    checkPairs(find_prosite);
                    i++;
                    break;
                case "--to_csv":
                    to_csv = args[i + 1];
                    checkPairs(to_csv);
                    tcsv = true;
                    i++;
                    break;
                case "--find_regex":
                    find_regex = args[i + 1];
                    checkPairs(find_regex);
                    i++;
                    break;
                case "--find_id":
                    find_id = args[i + 1];
                    checkPairs(find_id);
                    i++;
                    break;
                case "--find_description":
                    find_description = args[i + 1];
                    checkPairs(find_description);
                    i++;
                    break;
                case "--help":
                    help = true;
                    break;
                case "--find_organism":
                    find_organism = args[i + 1];
                    checkPairs(find_organism);
                    i++;
                    break;                  
            }
        }

                
        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Index out of bound, you are not giving correct input");
            System.exit(0);
        }

        if (input == null) {
            System.out.println("No input has been given");
            System.exit(0);
        } 
        else {
            CheckFileType(input);
            readFile(input);
        }             
    
        startApp();     
    }
      

    /**
     * 
     * starts the application.
     * 
     */    
    public void startApp(){
        String fheader = null;
        String fsequence = null;
        String forganism = null;
        String fid = null;
        String fsequencetype = null;
        String fname = null;
        String found_help = null;
        String found_summary = null;
        
        List<String> found_regex_list = new ArrayList<String>();
        List<String> found_id_list = new ArrayList<String>();
        List<String> found_prosite_list = new ArrayList<String>();
        List<String> found_organism_list = new ArrayList<String>();
        List<String> found_description_list = new ArrayList<String>();
        
        if(help){
            getHelp();  
        }        
        
        for(FastaEntry f: fastaEntries){
            fheader = f.header;
            fsequence = f.sequence;
            forganism = f.organism;
            fid = f.id;
            sequencetype = f.sequencetype;
            fname =f.name;

            totallength = totallength + fsequence.length();

            String found_regex = null;
            String found_prosite = null;
            String found_id = null;
            String found_organism = null;
            String found_description = null;                     
            
            if(find_regex != null){
                found_regex = findRegex(fid, fname, forganism, sequencetype, fsequence, find_regex);
                if (found_regex != ""){       
                    found_regex_list.add(found_regex);                
                }
            }        
            if(find_prosite != null){
                found_prosite = getProsite(fid, fname, forganism, sequencetype, fsequence, find_prosite);
                if (found_prosite != ""){
                    found_prosite_list.add(found_prosite);
                }
            }            
            
            if (find_id != null){
                found_id = findId(fid, fname, forganism, sequencetype, fsequence, fheader, find_id);
                if (found_prosite != ""){
                    found_id_list.add(found_id);
                }
            }
                  
            if (find_organism != null){
                found_organism = findOrganism(fheader, fsequence, forganism, find_organism);
                found_organism_list.add(found_organism);
            }
            
            if (find_description != null){
                found_description = getDescription(fheader, fsequence, fname, find_description);
                found_description_list.add(found_description);
            }     

        }
        if(summary){
            getSummary();}
            
        if(tcsv != false){
            to_CSV(to_csv, found_regex_list, found_prosite_list);
        }
        if(id){
            String text = String.format("These are the organisms with the ID[%s] you are looking for:", find_id);
            printToScreen(found_id_list, text);
        }
        if(id != true & find_id != null){
            System.out.println(String.format("Could not find the id [%s] you are looking for", find_id));        
        }
        if(organism){
            String text = String.format("These organisms fit the [%s] you are looking for:", find_organism);
            printToScreen(found_organism_list, text);
        }
        if(organism != true & find_organism != null){
            System.out.println(String.format("Could not find the organism [%s] you are looking for", find_organism));        
        }        
        if(description){
            String text = String.format("These organisms fit the discription [%s] you are looking for:", find_description);
            printToScreen(found_description_list, text);
        }
        if(description != true & find_description != null){
            System.out.println(String.format("Could not find the description [%s] you are looking for", find_description));        
        }        
        if(regex){
            String text = String.format("These organisms fit the regex pattern [%s] you are looking for:", find_regex);
            printToScreen(found_regex_list, text);
        }
        if(regex != true & find_regex != null){
            System.out.println(String.format("Could not find the regex pattern [%s] you are looking for", find_regex));        
        }
        if(prosites){
            String text = String.format("These organisms fit the prosite [%s] you are looking for:", find_prosite);
            printToScreen(found_prosite_list, text);
        }        
        if(pprosite != true & find_prosite != null){
            System.out.println(String.format("Could not find the prosite [%s] you are looking for", find_prosite));        
        }
    }

    /**
     *
     * @param input Check if the inputfile is a fasta file (fasta|faa|fa)
     *
     */
    private void CheckFileType(String input) {

        Pattern fastafiletype = Pattern.compile("Fasta|faa|fa");
        Matcher matchfastafiletype = fastafiletype.matcher(input);
        boolean matchfasta = matchfastafiletype.find();

        if (matchfasta) {
            file = input;
            System.out.println("\nProcessing file: "+ file);
        } 
        
        else {
            System.out.println("input must be a fastafile (.fasta, .fa, .faa)");
            System.exit(0);
        }
    }

    /**
     *
     * @param input Read the inputfile
     * 
     */
    private void readFile(String input) {
        File file = new File(input);
        StringBuilder value;
        try {
            
            String line;
            BufferedReader in = new BufferedReader(new FileReader(file));
            FastaEntry fa;
            StringBuilder sequenceBuilder = null;
            boolean isFirstEntry = true;
            String header = null;
            
            while ((line = in.readLine()) != null) {
                if (line.startsWith(">")) {
                    numberofsequences ++;
                   
                    if (!isFirstEntry){
                        fastaEntries.add(new FastaEntry(header, sequenceBuilder.toString()));

                    }
                    else{
                        isFirstEntry = false;
                    }
                    
                    header = line;
                    sequenceBuilder = new StringBuilder();
                } 
                else {
                    sequenceBuilder.append(line);
                }
            }
            
            fastaEntries.add(new FastaEntry(header, sequenceBuilder.toString()));
            
        } 
        catch (IOException e) {
            e.printStackTrace();
        }        
    }
    
     /**
     *
     * @param FastaEntry id (fid)
     * @param FastaEntry name
     * @param FastaEntry organism
     * @param FastaEntry sequencetype
     * @param FastaEntry sequence
     * @param command line prosite
     */
    public String getProsite(String fid, String fname, String forganism, String sequencetype, String fsequence, String find_prosite){
        List<Integer> prositeMatches = new ArrayList<Integer>();
        Pattern pattern = Pattern.compile(find_prosite);
        Matcher m = pattern.matcher(fsequence);
        String return_text = null;
        String match_sequence = null;

        if (m.find()){
            prosites = true;
            while (m.find()) {
                match_sequence = m.group(0);
                prositeMatches.add(m.start());
            }
        }
        else{
            pprosite = false;
            return return_text = "";
        }
        
        String prositematch = prositeMatches.toString();        
        return_text = String.format("%s%s&s%s%s%s%s%s%s%s%s%s", 
                fid, to_csv, fname, to_csv, forganism, to_csv, sequencetype, to_csv, prositematch, to_csv, match_sequence);
        
        return return_text;
    } 
    
    
     /**
     *
     * @param commandline csv character saparater
     * @param list of found regex organisms from fasta
     * @oaran list of found prosites in organisms
     */    
    public void to_CSV(String to_csv, List found_regex_list, List found_prosite_list){
        String print = null;       
        String csvfilename = null;
            Pattern pattern = Pattern.compile("(.*)(.fa)");
            Matcher m = pattern.matcher(input);
        
            if(m.find()){      
                csvfilename = m.group(1);
            }
            
        System.out.println(String.format("Writing file to: %s.csv", csvfilename));
        FileWriter fileWriter = null;

        try
	{   
            
            fileWriter = new FileWriter(String.format("%s.csv", csvfilename));
        
        }
	catch(IOException e)
	{
            System.out.println("Error when making csv-file");
	     e.printStackTrace();
	} 
        
        try{
            if(! found_regex_list.isEmpty()){
                fileWriter.append(String.format("ACCNO%sNAME%sORGANISM%sTYPE%sPOSITION%sREGEXSEQ\n", to_csv, to_csv, to_csv, to_csv, to_csv));
            }
            for (int i = 0 ; i < found_regex_list.size(); i++){
                print = found_regex_list.get(i).toString();
                fileWriter.append(print + "\n");
            }
            if(! found_prosite_list.isEmpty()){
            fileWriter.append(String.format("ACCNO%sNAME%sORGANISM%sTYPE%sPOSITION%sPROSITESEQ\n", to_csv, to_csv, to_csv, to_csv, to_csv));
            }
            for (int i = 0 ; i < found_prosite_list.size(); i++){
                print = found_prosite_list.get(i).toString();
                fileWriter.append(print + "\n");
            }
        }
        catch(IOException e){
            System.out.println("Error when making csv-file");
	     e.printStackTrace();
        }
                
        try{
            fileWriter.flush();
	    fileWriter.close();
        }
        catch(IOException e){
            System.out.println("Error when making csv-file");
	    e.printStackTrace();
        }
    }
  
     /**
     *
     * @param FastaEntry id (fid)
     * @param FastaEntry name
     * @param FastaEntry organism
     * @param FastaEntry sequencetype
     * @param FastaEntry sequence
     * @param commandline regex
     */
    public String findRegex(String fid, String fname, String forganism, String sequencetype, String fsequence, String find_regex){
        List<Integer> regexMatches = new ArrayList<Integer>();
        Pattern pattern = Pattern.compile(find_regex);
        Matcher m = pattern.matcher(fsequence);
        String return_text = null;
        String match_sequence = null;
        if(m.find()){
            
            while (m.find()) {
              regex = true;
              match_sequence = m.group(0);
              regexMatches.add(m.start());
            }
        }
        else{
            regex = false;
            return return_text = "";
        }
        String regexmatch = regexMatches.toString();
        return_text = String.format("%s%s&s%s%s%s%s%s%s%s%s%s", 
                fid, to_csv, fname, to_csv, forganism, to_csv, sequencetype, to_csv, regexmatch, to_csv, match_sequence);
    return return_text;
    }
    
    
     /**
     *
     * @param FastaEntry header
     * @param FastaEntry sequence
     * @param FastaEntry name
     * @param commandline description 
     */
    public String getDescription(String fheader, String fsequence, String fname, String find_description){
        Pattern pattern = Pattern.compile(find_description);
        Matcher m = pattern.matcher(fname);
        String return_text = null;
        
        if(m.find()){
            description = true;
            return_text = String.format(fheader + "\n" + fsequence);
        }
        else{
            description = false;
            return_text = "";
            
        }         
        return return_text;    
    }

    /**
     * This is the help function that give the information needed to handle the program
     */
    private void getHelp(){
        System.out.println("This is the helper function. Beneath you will find the options you have to put in the commandline.");
        System.out.println("--to_csv: needs a seperation mark. usually ';'");
        System.out.println("--find_regex 'REGEX HERE' ");
        System.out.println("--find_prosite 'PROSITE HERE' ");
        System.out.println("--find_organism 'ORGANISM HERE' ");
        System.out.println("--find_id 'ID HERE' ");
        System.out.println("--find_description 'DESCRIPTION HERE' ");
        System.out.println("--infile: 'input file' ");
        System.out.println("--summary: Shows summary");
        System.out.println("behind the files that need an input (HERE) the input needs to be between double quotation marks");
    }
    
    
     /**
     *
     * @param FastaEntry header
     * @param FastaEntry sequence
     * @param FastaEntry organism
     * @param commandline organism 
     */    
    private String findOrganism(String fheader, String fsequence, String forganism, String find_organism){
        Pattern pattern = Pattern.compile(find_organism);
        Matcher m = pattern.matcher(forganism);
        String return_text = null;   
        if(m.find()){
            organism = true;
            return_text = String.format(fheader + "\n" + fsequence);
        }
        else{
            organism = false;
            return_text = ""; 
        }         
        return return_text;   
    }

    /**
    *
    * @param FastaEntry id (fid)
    * @param FastaEntry name
    * @param FastaEntry organism
    * @param FastaEntry sequencetype
    * @param FastaEntry header
    * @param commandline find_id 
    */
    private String findId(String fid, String fname, String forganism, String sequencetype, String fsequence, String fheader, String find_id){
        Pattern pattern = Pattern.compile(find_id);
        Matcher m = pattern.matcher(fid);
        String return_text = null;    
        if(m.find()){
            id = true;
            return_text = String.format("%s\n%s", fheader, fsequence );
        }
        else{            
            id = false;
            return_text = "";
        } 
        return return_text;      
    }
    /**
     * 
    * @param list of found element from regex or prosite
    * @param text that should be printed to the screen
    */
    public void printToScreen(List found_list, String text){
        String print = null;
        System.out.println(text);
        if (! found_list.isEmpty()){
        for (int i = 0 ; i < found_list.size(); i++){
            print = found_list.get(i).toString();
            System.out.println(print);
            }
        }
        else
            System.out.println("No results were found");
    }
    
    /**
     * 
     * prints the summary
     */
    public void getSummary(){
        getAverageLength();
        System.out.println(String.format("file: %-10s\nSequence type: %-10s\nNumber of sequences: %-10s\nAverage sequence length: %-10s\n", file, sequencetype, numberofsequences, averagelength));
            }   
    
    /**
     * checks the pairs in the command line
     * @param arg 
     */
    public void checkPairs(String arg){
        Pattern pattern = Pattern.compile(".*");
        Matcher m = pattern.matcher(arg);
        Pattern patternstart = Pattern.compile("--");
        Matcher s = patternstart.matcher(arg);
        if(arg == null || arg == ""){    
            System.out.println("You have incorrect input in the command line, use --help for information");
            System.exit(0);            
        }
        if(s.find()){
            System.out.println("You probably forgot to fill in an argument, use --help for information"); 
            System.exit(0);
        }
        else{            
            legitinput = true;
        }        
    }
    
    /**
     * 
     * Calculates the average length of the sequences in the fastafile
     */
    public void getAverageLength(){
        averagelength = (totallength / numberofsequences);
    }
   
   
}