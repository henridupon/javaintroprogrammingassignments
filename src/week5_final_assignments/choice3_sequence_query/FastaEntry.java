
package week5_final_assignments.choice3_sequence_query;

/*
 * Copyright (c) Henri du Pon
 * Email Henridupon@gmail.com
 * All rights reserved.
 *
 * Final assignment 3: Developing a Fasta sequence query app 
 * Special challenge of this assignment: pattern search and applying filters 
 *
 */

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import static java.util.Objects.requireNonNull;

/**
 *
 * @author henridupon
 */
public class FastaEntry {
    public final String header;
    public final String sequence;
    public String organism;
    public String id;
    public String description;
    public String sequencetype;
    public String name;
    
    public FastaEntry(String header, String sequence){
        
        this.header = requireNonNull(header);
        this.sequence = requireNonNull(sequence);    
        this.getSequenceType(sequence);
        this.getOrganism(header);
        this.getID(header);
        this.getSequence(sequence);
        this.getName(header);
        
    }
    
    /**
     * 
     * @param header
     * 
     */ 
      public void getOrganism(String header){
        Pattern pattern = Pattern.compile("\\[(.*)\\]");
        Matcher m = pattern.matcher(header);
        
        if(m.find()){
            String organ = m.group(1);
            organism = m.group(1);
        }
        else{
            organism = "unknown";
        }        
    }
    
      /**
       * 
       * @param sequence
       * @return sequence
       */
    public String getSequence(String sequence){
        return sequence;
    }
    
    /**
     * 
     * @param header
     * @return id
     */
    public String getID(String header){
        Pattern pattern = Pattern.compile("(\\w*\\|(\\w*\\d*)\\|)");
        Matcher m = pattern.matcher(header);
        
        if(m.find()){      
            id = m.group(0);
        }
        else{    
            id = "unknown";
        }
        return id;
    }
    
    /**
     * 
     * @returns description 
     */
    public String getDescription(){
        return description;
    }
    
    /**
     * 
     * @param sequence
     * @return sequencetype
     */
    public String getSequenceType(String sequence){
                
        if(sequence.matches(".*[BDEFHIJKLMNOPQRSUVWXYZ].*")){       
            sequencetype = "PROTEIN";
        }
            else{
            sequencetype = "DNA";
        }           
        return sequencetype;
            
    }
    
    /**
     * @param header
     * returns the header
     */
    public String getHeader(String header){
        return header;
    }
    
    /**
     * 
     * @param header
     * @returns the name 
     */
    public String getName(String header){
        Pattern pattern = Pattern.compile("\\|(\\D*)\\[");
        Matcher m = pattern.matcher(header);
        
        if(m.find()){      
            name = m.group(1);
        }
        else{    
            name = "unknown";
        }
        return name;
    }
    /**
     * 
     * overrides the toString() command
     * @returns a string with the parameters: header, sequence, sequencetype, organism, id
     */
    @Override
    public String toString(){
        return String.format("FastaEntry [header: %s, sequence: %s, aminotype: %s, organism: %s, id: %s \n]", header, sequence, sequencetype, organism, id);
    }


}   